package de.mobilesol.matebet.ws.dto;

public class UserProfileDTO {

	public String id;
	public String name, email;
	public String picture;
	public String age;

	@Override
	public String toString() {
		return "UserProfileDTO [id=" + id + ", name=" + name + ", email="
				+ email + ", picture=" + picture + "]";
	}
}
