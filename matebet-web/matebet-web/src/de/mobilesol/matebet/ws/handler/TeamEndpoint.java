package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.io.Reader;
import java.util.Collection;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.TeamDTO;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.master.TeamService;

public class TeamEndpoint {

	private static final long serialVersionUID = 1L;

	private TeamService teamService = new TeamService();

	public TeamDTO handleGet(int id, HttpServletResponse resp)
			throws IOException {
		TeamDTO u = teamService.getTeam(id);
		if (u == null) {
			throw new ResourceNotFoundException(700,
					"Team with id " + id + " not found");
		}
		return u;

	}

	public Collection<TeamDTO> handleGetAll(HttpServletResponse resp)
			throws IOException {
		Collection<TeamDTO> l = teamService.getAllTeams();
		return l;
	}

	public Object handleAdd(Reader in, HttpServletResponse resp)
			throws IOException {

		Gson gson = new Gson();
		TeamDTO u = gson.fromJson(in, TeamDTO.class);
		int id = teamService.addTeam(u);
		return id;
	}
}
