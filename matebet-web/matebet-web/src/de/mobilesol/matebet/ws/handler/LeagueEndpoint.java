package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.service.master.LeagueService;

public class LeagueEndpoint {

	private static final long serialVersionUID = 1L;

	private LeagueService leagueService = new LeagueService();

	public List<LeagueDTO> handleGetAllLeagues(HttpServletResponse resp,
			boolean incDisabled) throws IOException {
		List<LeagueDTO> u = leagueService.getAllLeagues(incDisabled);
		return u;
	}

	public LeagueDTO deleteLeague(int leagueId, HttpServletResponse resp)
			throws IOException {
		LeagueDTO u = leagueService.deleteLeague(leagueId);
		return u;
	}

	public LeagueDTO disableLeague(int id, boolean disable) {
		LeagueDTO l = leagueService.disableLeague(id, disable);
		return l;
	}
}
