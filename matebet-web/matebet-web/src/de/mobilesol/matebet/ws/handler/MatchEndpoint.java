package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.master.MatchService;

public class MatchEndpoint {

	private static final long serialVersionUID = 1L;

	private MatchService matchService = new MatchService();

	public MatchDTO handleGetMatch(int groupId, HttpServletResponse resp)
			throws IOException {
		MatchDTO u = matchService.getMatch(groupId);
		if (u == null) {
			throw new ResourceNotFoundException(500,
					"Match with id " + groupId + " not found");
		}
		return u;
	}

	public List<MatchDTO> handleGetMatchesByGroup(int groupId,
			HttpServletResponse resp) throws IOException {
		List<MatchDTO> u = matchService.getMatchesByGroup(groupId);
		if (u == null) {
			throw new ResourceNotFoundException(501,
					"Match with id " + groupId + " not found");
		}
		return u;
	}
}
