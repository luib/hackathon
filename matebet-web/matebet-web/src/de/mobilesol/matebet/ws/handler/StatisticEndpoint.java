package de.mobilesol.matebet.ws.handler;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import de.mobilesol.matebet.web.dto.StatisticDTO;
import de.mobilesol.matebet.web.service.StatisticService;

public class StatisticEndpoint {

	private static final long serialVersionUID = 1L;

	private StatisticService statService = new StatisticService();

	public Object handleGetStatistic(int userId, HttpServletResponse resp)
			throws IOException {
		StatisticDTO s = statService.getStatisctic(userId);
		return s;
	}
}
