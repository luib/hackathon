package de.mobilesol.matebet.web.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetResultDTO;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;

public class CalculatePointsUtil {
	private static final Logger log = Logger
			.getLogger(CalculatePointsUtil.class.getName());

	private boolean failed;

	public boolean isFailed() {
		return failed;
	}

	public Map<Integer, Map<Integer, Integer>> calculatePoints(BetDTO bet,
			MatchTipDTO ut, TipsByMatchByGroupMap resultByMatch) {

		failed = false;
		Map<Integer, Map<Integer, Integer>> result = new HashMap<Integer, Map<Integer, Integer>>();

		Set<Integer> allMatches = new HashSet<Integer>();
		for (BetGroupDTO g : bet.groups) {
			for (BetMatchDTO m : g.matches) {
				allMatches.add(m.matchId);
			}
		}
		for (int matchId : allMatches) {
			Map<Integer, Integer> map = new HashMap<Integer, Integer>();
			result.put(matchId, map);

			for (Integer userId : bet.userIds) {
				log.info("handle user " + userId);
				TipsByMatchByGroupMap groupTipsUser = ut.groupsByUser
						.get(userId);
				Map<Integer, Integer> userPointByMatch = calculatePointsByUser(
						bet, groupTipsUser, resultByMatch);

				Integer i1 = (userPointByMatch != null
						? userPointByMatch.get(matchId)
						: null);

				if (i1 != null) {
					map.put(userId, i1);
				}
			}
		}

		return result;
	}

	Map<Integer, Integer> calculatePointsByUser(BetDTO bet,
			TipsByMatchByGroupMap groupTipsUser,
			TipsByMatchByGroupMap resultByMatch) {

		log.debug("calculatePoints user=" + groupTipsUser);
		log.debug("calculatePoints result=" + resultByMatch);

		GregorianCalendar cal = new GregorianCalendar();

		Map<Integer, Integer> pointsByMatch = new HashMap<Integer, Integer>();
		for (BetGroupDTO group : bet.groups) {
			log.info("handle group " + group.name + ";" + group.groupId);

			List<BetMatchDTO> matches = group.matches;
			for (BetMatchDTO m : matches) {
				log.debug("handle match " + m.matchId);
				TipsByMatchMap userMaps = null;
				if (groupTipsUser != null) {
					userMaps = groupTipsUser.groups.get(group.groupId);
				}

				try {
					TipsByMatchMap rm = (resultByMatch != null
							? resultByMatch.groups.get(group.groupId)
							: null);
					pointsByMatch.put(m.matchId,
							(calculatePointsByMatch(m.matchId, userMaps, rm)));
				} catch (IllegalStateException ex) {
					// do not set result
					long waitingForMinutes = (cal.getTime().getTime()
							- m.matchdate.getTime()) / 1000 / 60;
					if (waitingForMinutes > 240) {
						// matchresult has not been entered after 4h
						log.warn(
								"*** match " + m + " is waiting for result for "
										+ waitingForMinutes + " minutes; "
										+ ex.getMessage());
						if (waitingForMinutes > 60 * 24) {
							// send mail after 24h
							// new SendMail().send("mateBet Warning",
							// "Waiting for match result " + m, "lui",
							// "lui.baeumer@gmail.com", null);
						}
					} else if (waitingForMinutes > 120) {
						// matchresult has not been entered after 120min
						log.info("match " + m + " is waiting for result for "
								+ waitingForMinutes + " minutes; "
								+ ex.getMessage());
					} else {
						log.info("match " + m + " is will start in "
								+ (-waitingForMinutes) + " minutes.");
					}
					failed = true;
				}
			}
		}
		return pointsByMatch;
	}

	int calculatePointsByMatch(int matchId, TipsByMatchMap myTips,
			TipsByMatchMap resultTips) {
		if (resultTips == null || resultTips.tips == null
				|| resultTips.tips.get(matchId) == null) {
			throw new IllegalStateException(matchId + " is null; "
					+ (resultTips == null
							? "resultTips=null"
							: (resultTips.tips == null
									? "resultTips.tips=null"
									: "only matches: "
											+ resultTips.tips.keySet())));
		}
		if (myTips == null || myTips.tips == null
				|| myTips.tips.get(matchId) == null) {
			return 0;
		}

		TipDTO resultTip = resultTips.tips.get(matchId);
		TipDTO myTip = myTips.tips.get(matchId);

		if (resultTip == null || myTip == null) {
			return 0;
		}

		// exact tip
		if (resultTip.tipTeam1 == myTip.tipTeam1
				&& resultTip.tipTeam2 == myTip.tipTeam2) {
			return 3;
		}

		// both remis
		int a = (resultTip.tipTeam1 - resultTip.tipTeam2);
		int b = (myTip.tipTeam1 - myTip.tipTeam2);

		if (a == b) {
			return 2;
		}

		if (a == 0 && b == 0) {
			return 1;
		}

		// tendency correct
		if (a < 0 && b < 0 || a > 0 && b > 0) {
			return 1;
		}

		return 0;
	}

	public List<BetResultDTO> getResults(
			Map<Integer, Map<Integer, Integer>> resultByMatchByUser) {

		Map<Integer, Integer> pointsByUser = new HashMap<Integer, Integer>();

		List<BetResultDTO> res = new ArrayList<BetDTO.BetResultDTO>();

		for (Map.Entry<Integer, Map<Integer, Integer>> s : resultByMatchByUser
				.entrySet()) {

			Map<Integer, Integer> resultByUser = s.getValue();
			for (Map.Entry<Integer, Integer> e : resultByUser.entrySet()) {
				Integer points = pointsByUser.get(e.getKey());
				pointsByUser.put(e.getKey(),
						e.getValue() + (points != null ? points : 0));
			}
		}

		for (Map.Entry<Integer, Integer> s : pointsByUser.entrySet()) {
			BetResultDTO br = new BetResultDTO();
			br.userId = s.getKey();
			br.result = (s.getValue() != null ? s.getValue() : 0);
			res.add(br);
		}

		Collections.sort(res, new Comparator<BetResultDTO>() {

			@Override
			public int compare(BetResultDTO o1, BetResultDTO o2) {
				int r = o2.result - o1.result;
				if (r != 0) {
					return r;
				}
				return o1.userId - o2.userId;
			}
		});

		return res;
	}

	public Map<Integer, Integer> getResultByUser(List<Integer> userIds,
			List<BetResultDTO> result) {
		log.info("getResultByUser(" + userIds + ", " + result + ")");
		Map<Integer, Integer> resultByUser = new HashMap<Integer, Integer>();
		for (int userId : userIds) {
			resultByUser.put(userId, getResult(userId, result));
		}
		return resultByUser;
	}

	private int getResult(int userId, List<BetResultDTO> result) {
		// all results are equal
		if (result.get(0).result == result.get(result.size() - 1).result) {
			return 0;
		}

		// criteria to win
		int i = 1;
		while (i < result.size()
				&& result.get(0).result == result.get(i).result) {
			i++;
		}
		for (int j = 0; j < i; j++) {
			if (result.get(j).userId == userId) {
				log.info("you win");
				return result.get(0).result
						- result.get(result.size() - 1).result;
			}
		}

		// criteria to loose
		i = result.size() - 2;
		while (i >= 0 && result.get(result.size() - 1).result == result
				.get(i).result) {
			i--;
		}
		for (int j = result.size() - 1; j > i; j--) {
			if (result.get(j).userId == userId) {
				log.info("you loose");
				return result.get(result.size() - 1).result
						- result.get(0).result;
			}
		}

		return 0;
	}
}
