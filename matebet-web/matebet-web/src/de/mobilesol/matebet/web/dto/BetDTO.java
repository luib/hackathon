package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;

public class BetDTO implements Serializable {

	private static final long serialVersionUID = -6848468600687152140L;

	public static enum BetStatus {
		INITIATED(0), RUNNING(1), CLOSED(2), FINISHED(3), DECLINED(4);
		public int value;

		private BetStatus(int value) {
			this.value = value;
		}
	};

	public int betId;

	public List<Integer> userIds;
	public Map<Integer, String> userNames;

	public int leagueId;
	public String leagueName;
	public List<BetGroupDTO> groups;
	public String title;
	public Date created;

	public String stake;

	public BetStatus status;

	// result is set if bet is finished
	public List<BetResultDTO> results;
	public Map<Integer, Integer> resultByUser;

	// tmpResult will be set if bet is closed
	public List<BetResultDTO> tmpResults;

	public Map<Integer, Map<Integer, Integer>> resultByMatchByUser;

	public Date lastMatchdate;
	public Map<Integer, Boolean> confirmedBy;

	// only filled after Status is Closed/Finished
	public MatchTipDTO matchTips;

	// only filled after Status is Finished
	public boolean redeemed;
	public TipsByMatchByGroupMap resultTips;
	public Map<Integer, Boolean> archivedBy;

	// only filled by ActiveBetStatus Service
	public ActiveBetStatusDTO activeStatus;

	@Override
	public String toString() {
		return "BetDTO [id=" + betId + ", title=" + title + ", users=" + userIds
				+ ", leagueId=" + leagueId + ", status=" + status + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + betId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BetDTO other = (BetDTO) obj;
		if (betId != other.betId)
			return false;
		return true;
	}

	public static class BetResultDTO implements Serializable {

		private static final long serialVersionUID = -4029059979819911233L;

		public int userId, result;

		@Override
		public String toString() {
			return "BetResultDTO [userId=" + userId + ", result=" + result
					+ "]";
		}
	}
}
