package de.mobilesol.matebet.web.dto.contact;

import java.io.Serializable;
import java.util.List;

public class ContactDTO implements Serializable {

	private static final long serialVersionUID = 8460766653549101644L;

	public int id;
	public String rawId, displayName;

	public ContactName name;

	public String nickname;

	public List<ContactEmail> emails;

	@Override
	public String toString() {
		return "ContactDTO [displayName=" + displayName + ", name=" + name
				+ ", nickname=" + nickname + ", emails=" + emails + "]";
	}

	public static class ContactName implements Serializable {

		private static final long serialVersionUID = -6447759717138790241L;

		public String familyName, givenName, formatted;

		@Override
		public String toString() {
			return "ContactName [familyName=" + familyName + ", givenName="
					+ givenName + ", formatted=" + formatted + "]";
		}
	}

	public static class ContactEmail {
		public String id;
		public boolean pref;
		public String value, type;

		@Override
		public String toString() {
			return value;
		}
	}
}
