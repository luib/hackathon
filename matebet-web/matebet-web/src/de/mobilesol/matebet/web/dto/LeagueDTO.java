package de.mobilesol.matebet.web.dto;

import java.io.Serializable;

public class LeagueDTO implements Serializable {

	private static final long serialVersionUID = -683472949501206384L;

	public int leagueId;
	public String title, saison, shortName;
	public Boolean disabled;

	@Override
	public String toString() {
		return "LeagueDTO [leagueId=" + leagueId + ", title=" + title
				+ ", saison=" + saison + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + leagueId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LeagueDTO other = (LeagueDTO) obj;
		if (leagueId != other.leagueId)
			return false;
		return true;
	}
}
