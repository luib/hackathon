package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StatisticDTO implements Serializable {

	private static final long serialVersionUID = -541059642217143007L;

	public List<BetDTO> waitingBets = new ArrayList<BetDTO>();
	public List<BetDTO> wonBets = new ArrayList<BetDTO>();
	public List<BetDTO> lostBets = new ArrayList<BetDTO>();
	public List<BetDTO> remisBets = new ArrayList<BetDTO>();

	public int wonBetsCount, lostBetsCount, remisBetsCount;

	@Override
	public String toString() {
		return "StatisticDTO [waitingBets=" + waitingBets.size() + ", wonBets="
				+ wonBets.size() + ", lostBets=" + lostBets.size()
				+ ", remisBets=" + remisBets.size() + ", wonBetsCount="
				+ wonBetsCount + ", lostBetsCount=" + lostBetsCount
				+ ", remisBetsCount=" + remisBetsCount + "]";
	}
}
