package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.Date;

public class BetMatchDTO implements Serializable {

	private static final long serialVersionUID = -8051248212563353740L;

	public int matchId;
	public Date matchdate;
	public TeamDTO team1, team2;

	@Override
	public String toString() {
		return "BetMatchDTO [matchId=" + matchId + ", match=" + team1 + "-"
				+ team2 + "]";
	}
}
