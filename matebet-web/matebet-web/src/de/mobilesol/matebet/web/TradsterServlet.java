package de.mobilesol.matebet.web;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.service.ActiveBetService;




 
public class TradsterServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Logger log = Logger.getLogger( TradsterServlet.class );
	
	{ BasicConfigurator.configure(); }
	
	static double data = 10;
	
	static void setData(double d) { data = d;
	
	}
	
	private void o(String str) { System.out.println(str);}
	
@Override
   public void doGet(HttpServletRequest request, HttpServletResponse response)
               throws IOException, ServletException {
	
		ActiveBetService s = new ActiveBetService();
		int[] rx = s.countStats();
		setData(rx[0] - rx[1]);
	
      // Set the response message's MIME type.
      response.setContentType("text/html;charset=UTF-8");
      // Allocate a output writer to write the response message into the network socket.
      PrintWriter out = response.getWriter();
      
      String relativeWebPath = "header.html";
      String absoluteDiskPathHeader = getServletContext().getRealPath(relativeWebPath);
      log.info("path="+absoluteDiskPathHeader);
      
      relativeWebPath = "end.html";
      String absoluteDiskPathEnd = getServletContext().getRealPath(relativeWebPath);
      log.info("path="+absoluteDiskPathEnd);
   
   
      
      BufferedReader header = new BufferedReader(new FileReader(absoluteDiskPathHeader));
      BufferedReader end = new BufferedReader(new FileReader(absoluteDiskPathEnd));
      
      String r="";
      
	   	String inputLine;
		while ((inputLine = header.readLine()) != null) {
			r = r+inputLine+"\n";
		}
		header.close();
		
	    String dyn = "data.push({x:1, y:1, z: "+ data + ", style:value})";
		
		r = r + dyn +  "\n";
		

		while ((inputLine = end.readLine()) != null) {
			r = r+inputLine+"\n";
		}
		end.close();		
		
		//log.info("h="+h);
 
		out.print(r);
 
      // Write the response message, in an HTML document.
      try {
         
      } finally {
         out.close();  // Always close the output writer
      }
   }







  

   
}