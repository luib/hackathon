package de.mobilesol.matebet.web.cron.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.appengine.api.taskqueue.Queue;
import com.google.appengine.api.taskqueue.QueueFactory;
import com.google.appengine.api.taskqueue.TaskOptions;

public class MissingResultEnteredServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(MissingResultEnteredServlet.class.getName());

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		Queue queue = QueueFactory.getQueue("default");
		TaskOptions b = TaskOptions.Builder.withUrl("/_ah/queue/missingresult");
		if (req.getParameter("days") != null) {
			b = b.param("days", req.getParameter("days"));
		}
		queue.add(b);

		resp.getWriter().write(
				"kicked off asynchronous job. Check your email in some minutes");
	}
}
