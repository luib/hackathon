package de.mobilesol.matebet.web.dao;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetResultDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;

public class BetDAO {
	private static final Logger log = Logger.getLogger(BetDAO.class.getName());
	private static final String TABLE = "bet_t";
	private ShardedCounter shard;

	public BetDAO() {
		shard = new ShardedCounter("bet_s", 100000);
	}

	public int addBet(BetDTO a) {
		log.info("addBet" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		a.betId = shard.getCount();
		shard.increment();
		Key key = KeyFactory.createKey(TABLE, a.betId);
		try {
			datastore.get(key);
			throw new IllegalStateException(
					"id " + a.betId + " already exists,");
		} catch (EntityNotFoundException e1) {
			a.status = BetStatus.INITIATED;
			a.created = new Date();
			Entity e = toEntity(new Entity(key), a);
			datastore.put(e);

			for (int userId : a.userIds) {
				addUserBet(userId, a.betId, datastore);
			}

			return a.betId;
		}
	}

	public BetDTO updateBet(BetDTO a) throws EntityNotFoundException {
		log.info("updateBet(" + a + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.betId);
		Entity e = toEntity(datastore.get(key), a);
		e.setProperty("modified", new Date());
		datastore.put(e);

		return toObject(e);
	}

	private void addUserBet(int userid, int betId, DatastoreService datastore) {
		Key key = KeyFactory.createKey("userbet_t", userid);
		Gson gson = new Gson();

		Set<Integer> list1;
		Entity e;
		try {
			e = datastore.get(key);

			String list = (String) e.getProperty("bets");
			if (list != null) {
				Type ab = new TypeToken<Set<Integer>>() {
				}.getType();
				list1 = gson.fromJson(list, ab);
				list1.add(betId);
				e.setProperty("bets", gson.toJson(list1));
			}
		} catch (EntityNotFoundException e1) {
			e = new Entity(key);
			e.setProperty("bets", gson.toJson(Collections.singleton(betId)));
		}
		datastore.put(e);
	}

	public BetDTO confirmBet(int betId, int opponentId, boolean confirm)
			throws EntityNotFoundException {
		log.info("confirmBet(" + betId + "," + opponentId + ", " + confirm
				+ ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, betId);
		Entity e = datastore.get(key);

		String status = (String) e.getProperty("status");
		if (BetStatus.INITIATED.name().equals(status)) {

			e.setProperty("modified", new Date());
			e.setProperty("status", BetStatus.RUNNING.name());

			// set confirmed
			Type ab = new TypeToken<Map<Integer, Boolean>>() {
			}.getType();
			String confirmedBy = (String) e.getProperty("confirmedBy");
			Gson gson = new Gson();
			Map<Integer, Boolean> set;
			if (confirmedBy != null) {
				set = gson.fromJson(confirmedBy, ab);
			} else {
				set = new HashMap<Integer, Boolean>();
			}
			set.put(opponentId, confirm);

			e.setProperty("confirmedBy", gson.toJson(set));

			// check if all declined
			if (!confirm) {
				boolean allDeclined = true;
				BetDTO bet = toObject(e);
				for (int i = 1; i < bet.userIds.size(); i++) {
					int userId = bet.userIds.get(i);

					Boolean b = set.get(userId);
					log.info("user " + userId + " confirmed: " + b);
					if (b == null || b != null && b) {
						allDeclined = false;
					}
				}
				if (allDeclined) {
					log.info("all declined" + bet);
					return declineBet(betId);
				}
			}

			datastore.put(e);

			BetDTO bet = toObject(e);
			return bet;
		} else {
			return null;
		}
	}

	public void deleteBet(int betId) {
		log.info("deleteBet" + betId);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key keyOld = KeyFactory.createKey(TABLE, betId);
		datastore.delete(keyOld);
	}

	public BetDTO declineBet(int betId) {
		log.info("declineBet" + betId);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key keyOld = KeyFactory.createKey(TABLE, betId);
		Entity e;
		try {
			e = datastore.get(keyOld);
		} catch (EntityNotFoundException e1) {
			log.warn("entity " + betId + " not found");
			return null;
		}

		BetDTO bet = toObject(e);

		if (!BetStatus.INITIATED.name().equals(bet.status.name())) {
			return null;
		}

		log.info("bet is initiated. So archive and set status to declined.");
		bet.status = BetStatus.DECLINED;

		moveToClosedBets(bet, datastore);
		datastore.delete(keyOld);

		return bet;
	}

	private void moveToClosedBets(BetDTO bet, DatastoreService datastore) {
		Date now = new Date();
		Gson gson = new Gson();

		// set archived
		if (bet.archivedBy == null) {
			bet.archivedBy = new HashMap<Integer, Boolean>();
		}
		for (int userId : bet.userIds) {
			if (bet.archivedBy.get(userId) == null) {
				bet.archivedBy.put(userId, false);
			}
		}

		for (int userId : bet.userIds) {
			Key key1 = KeyFactory.createKey("betclosed_t",
					bet.betId + "/" + userId);

			Entity e = new Entity(key1);
			e.setProperty("userId", userId);
			e.setProperty("betId", bet.betId);
			e.setProperty("bet", new Text(gson.toJson(bet)));
			e.setProperty("created", now);
			datastore.put(e);
		}
	}

	public BetDTO getBet(int id) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, id);
		try {
			Entity e = datastore.get(key);
			BetDTO a = toObject(e);
			return a;
		} catch (EntityNotFoundException e) {
			return null;
		}
	}

	public List<BetDTO> getOpenBets(int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> list = new ArrayList<BetDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			BetDTO b = toObject(it.next());
			list.add(b);
		}

		Collections.sort(list, new Comparator<BetDTO>() {

			@Override
			public int compare(BetDTO arg1, BetDTO arg0) {
				long ret = arg0.created.getTime() - arg1.created.getTime();
				if (ret > 0) {
					return 1;
				} else if (ret < 0) {
					return -1;
				}

				return arg0.leagueId - arg0.leagueId;
			}
		});
		return list;
	}

	public List<BetDTO> getBetsByStatus(BetStatus status) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("status", FilterOperator.EQUAL,
				status.name()));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> list = new ArrayList<BetDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			BetDTO b = toObject(it.next());
			list.add(b);
		}

		return list;
	}

	public List<BetDTO> getBetsByLeague(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("leagueId", FilterOperator.EQUAL,
				leagueId));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> list = new ArrayList<BetDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			BetDTO b = toObject(it.next());
			if (b.status == BetStatus.RUNNING) {
				list.add(b);
			}
		}

		return list;
	}

	public List<BetDTO> deleteBetsByLeague(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("leagueId", FilterOperator.EQUAL,
				leagueId));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> list = new ArrayList<BetDTO>();
		List<Entity> elist = new ArrayList<Entity>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			BetDTO b = toObject(e);
			list.add(b);

			e.setProperty("deleted", true);
			elist.add(e);
		}
		datastore.put(elist);

		return list;
	}

	public List<BetDTO> getBetsToBeClosed() {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		ArrayList<Filter> a = new ArrayList<Filter>();
		a.add(new FilterPredicate("status", FilterOperator.EQUAL,
				BetStatus.RUNNING.name()));
		a.add(new FilterPredicate("lastMatchDate", FilterOperator.LESS_THAN,
				System.currentTimeMillis()));
		q.setFilter(new CompositeFilter(CompositeFilterOperator.AND, a));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> bets = new ArrayList<BetDTO>();

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();

			BetDTO b = toObject(e);
			bets.add(b);
		}

		return bets;
	}

	public List<BetDTO> getBetsToBeDeclined() {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		long now = System.currentTimeMillis();

		Query q = new Query(TABLE);
		ArrayList<Filter> a = new ArrayList<Filter>();
		a.add(new FilterPredicate("status", FilterOperator.EQUAL,
				BetStatus.INITIATED.name()));
		a.add(new FilterPredicate("lastMatchDate", FilterOperator.LESS_THAN,
				now));
		q.setFilter(new CompositeFilter(CompositeFilterOperator.AND, a));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> bets = new ArrayList<BetDTO>();

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();

			BetDTO b = toObject(e);
			bets.add(b);
		}

		return bets;
	}

	public void setStatusClosed(BetDTO b, MatchTipDTO matchTips) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, b.betId);
		try {
			Entity e = datastore.get(key);

			Gson gson = new Gson();

			e.setProperty("status", BetStatus.CLOSED.name());
			e.setProperty("modified", new Date());
			e.setProperty("leagueName", b.leagueName);

			if (matchTips != null) {
				e.setProperty("matchTips", new Text(gson.toJson(matchTips)));
			}

			datastore.put(e);
		} catch (EntityNotFoundException e1) {
			log.warn("entity not found " + b);
			return;
		}
	}

	public void setStatusFinished(List<BetDTO> bets) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		for (BetDTO b : bets) {

			Key keyOld = KeyFactory.createKey(TABLE, b.betId);

			try {
				Entity e = datastore.get(keyOld);
				b.status = BetStatus.FINISHED;
				moveToClosedBets(b, datastore);
				datastore.delete(keyOld);
			} catch (EntityNotFoundException ex) {
				log.warn("bet with key " + keyOld + " does not exist.");
			}
		}
	}

	private Entity toEntity(Entity e, BetDTO a) {
		Gson gson = new GsonBuilder().setDateFormat(DateFormat.LONG).create();
		e.setProperty("lastMatchDate", a.lastMatchdate.getTime());
		e.setProperty("status", a.status.name());

		if (a.matchTips != null) {
			e.setProperty("matchTips", new Text(gson.toJson(a.matchTips)));
		}
		if (a.resultByMatchByUser != null) {
			e.setProperty("resultByMatchByUser",
					new Text(gson.toJson(a.resultByMatchByUser)));
		}
		if (a.resultTips != null) {
			e.setProperty("resultTips", new Text(gson.toJson(a.resultTips)));
		}

		if (a.confirmedBy != null) {
			e.setProperty("confirmedBy", gson.toJson(a.confirmedBy));
		}

		BaseBetDTO base = new BaseBetDTO();
		base.betId = a.betId;
		base.created = a.created;
		base.groups = a.groups;
		base.leagueId = a.leagueId;
		base.leagueName = a.leagueName;
		base.stake = a.stake;
		base.title = a.title;
		base.userIds = a.userIds;
		base.userNames = a.userNames;
		e.setProperty("master", new Text(gson.toJson(base)));

		return e;
	}

	private BetDTO toObject(Entity e) {
		BetDTO a = new BetDTO();

		Gson gson = new Gson();

		Text master = (Text) e.getProperty("master");
		BaseBetDTO m = gson.fromJson(master.getValue(), BaseBetDTO.class);
		a.betId = m.betId;
		a.created = m.created;
		a.groups = m.groups;
		a.leagueId = m.leagueId;
		a.leagueName = m.leagueName;
		a.stake = m.stake;
		a.title = m.title;
		a.userIds = m.userIds;
		a.userNames = m.userNames;

		Number nn = (Number) e.getProperty("lastMatchDate");
		a.lastMatchdate = new Date(nn.longValue());

		Object matchTips = e.getProperty("matchTips");
		if (matchTips != null) {
			String mt = (matchTips instanceof Text
					? ((Text) matchTips).getValue()
					: (String) matchTips);
			a.matchTips = gson.fromJson(mt, MatchTipDTO.class);
		}

		Object resultTips = e.getProperty("resultTips");
		if (resultTips != null) {
			String mt = (resultTips instanceof Text
					? ((Text) resultTips).getValue()
					: (String) resultTips);
			a.resultTips = gson.fromJson(mt, TipsByMatchByGroupMap.class);
		}

		Object resultByMatchByUser = e.getProperty("resultByMatchByUser");
		if (resultByMatchByUser != null) {

			Type ct = new TypeToken<Map<Integer, Map<Integer, Integer>>>() {
			}.getType();

			String mt = (resultByMatchByUser instanceof Text
					? ((Text) resultByMatchByUser).getValue()
					: (String) resultByMatchByUser);
			a.resultByMatchByUser = gson.fromJson(mt, ct);
		}

		a.status = BetStatus.valueOf((String) e.getProperty("status"));

		Type ab = new TypeToken<Map<Integer, Boolean>>() {
		}.getType();

		String confirmedBy = (String) e.getProperty("confirmedBy");
		if (confirmedBy != null) {
			try {
				a.confirmedBy = gson.fromJson(confirmedBy, ab);
			} catch (Exception exc) {
				a.confirmedBy = new HashMap<Integer, Boolean>();
			}
		} else {
			a.confirmedBy = new HashMap<Integer, Boolean>();
		}

		return a;
	}

	public static class BaseBetDTO {

		public int betId;

		public List<Integer> userIds;
		public Map<Integer, String> userNames;

		public int leagueId;
		public String leagueName;
		public List<BetGroupDTO> groups;
		public String title;
		public Date created;

		public String stake;
	}

	public static class BetClosedDTO {

		public BetStatus status;

		public List<BetResultDTO> results;
		public Map<Integer, Integer> resultByUser;

		public Map<Integer, Map<Integer, Integer>> resultByMatchByUser;

		public MatchTipDTO matchTips;
		public Map<Integer, Boolean> confirmedBy;

		public TipsByMatchByGroupMap resultTips;
	}
}
