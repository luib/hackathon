package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.BetDTO;

public class BetClosedDAO {
	private static final Logger log = Logger
			.getLogger(BetClosedDAO.class.getName());
	private static final String TABLE = "betclosed_t";

	public void repair() {
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			BetDTO bet = toObject(e);
			if (bet.archivedBy == null) {
				bet.archivedBy = new HashMap<Integer, Boolean>();
			}
			for (int userId : bet.userIds) {
				if (bet.archivedBy.get(userId) == null) {
					bet.archivedBy.put(userId, false);
				}
			}
			e = toEntity(e, bet);
			datastore.put(e);
		}
	}

	public BetDTO getBet(int betId, int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("betId", FilterOperator.EQUAL, betId));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			int user = ((Number) e.getProperty("userId")).intValue();
			if (user == userId) {
				BetDTO b = toObject(e);
				return b;
			}
		}

		return null;
	}

	public List<BetDTO> getClosedBets(int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));

		PreparedQuery pq = datastore.prepare(q);

		List<BetDTO> list = new ArrayList<BetDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			BetDTO b = toObject(it.next());
			list.add(b);
		}

		log.info("found " + list.size() + " entries");
		Collections.sort(list, new Comparator<BetDTO>() {

			@Override
			public int compare(BetDTO arg1, BetDTO arg0) {
				long ret = arg0.created.getTime() - arg1.created.getTime();
				if (ret > 0) {
					return 1;
				} else if (ret < 0) {
					return -1;
				}

				return arg0.leagueId - arg0.leagueId;
			}
		});

		return list;
	}

	public void setArchived(int betId, int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, betId + "/" + userId);
		try {
			Entity e = datastore.get(key);
			BetDTO bet = toObject(e);
			bet.archivedBy.put(userId, true);
			e = toEntity(e, bet);
			datastore.put(e);
		} catch (EntityNotFoundException ex) {
			log.warn("entity with key " + key + " does not exist.");
		}
	}

	public void delete(int betId, int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, betId + "/" + userId);
		datastore.delete(key);
	}

	private BetDTO toObject(Entity e) {
		Gson gson = new Gson();
		Object bet = e.getProperty("bet");
		BetDTO b = gson.fromJson(
				(bet instanceof Text ? ((Text) bet).getValue() : (String) bet),
				BetDTO.class);
		if (b == null) {
			log.warn("bet must not be null " + e.getProperty("betId"));
		}
		return b;
	}

	private Entity toEntity(Entity e, BetDTO bet) {
		Gson gson = new Gson();
		e.setProperty("bet", new Text(gson.toJson(bet)));
		return e;
	}
}
