package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.PushNotificationDTO;

public class PushHistoryDAO {
	private static final Logger log = Logger
			.getLogger(PushHistoryDAO.class.getName());
	private static final String TABLE = "pushhistory_t";

	public void addPushNotification(int userId, PushNotificationDTO push,
			String platform) {
		log.info("addPushNotification(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE,
				System.currentTimeMillis() + "/" + userId);
		Entity e = new Entity(key);
		e.setProperty("userId", userId);
		e.setProperty("platform", platform);
		e.setProperty("notification", new Text(new Gson().toJson(push)));
		e.setProperty("created", new Date());
		datastore.put(e);
	}

	public List<PushNotificationDTO> getPushNotifications(int userId) {
		log.info("getPushNotifications(" + userId + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(
				new FilterPredicate("userId", FilterOperator.EQUAL, userId));

		PreparedQuery pq = datastore.prepare(q);

		List<PushNotificationDTO> list = new ArrayList<PushNotificationDTO>();
		Iterator<Entity> it = pq.asIterator();
		Gson gson = new Gson();
		while (it.hasNext()) {
			Entity e = it.next();
			Text t = (Text) e.getProperty("notification");
			PushNotificationDTO p = gson.fromJson(t.getValue(),
					PushNotificationDTO.class);
			list.add(p);
		}
		return list;
	}

	public PushNotificationDTO getPushNotification(String key) {
		log.info("getPushNotification(" + key + ")");

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key dkey = KeyFactory.createKey(TABLE, key);

		Entity e;
		try {
			e = datastore.get(dkey);
			Gson gson = new Gson();
			Text t = (Text) e.getProperty("notification");
			PushNotificationDTO p = gson.fromJson(t.getValue(),
					PushNotificationDTO.class);
			return p;
		} catch (EntityNotFoundException e1) {
			return null;
		}
	}
}
