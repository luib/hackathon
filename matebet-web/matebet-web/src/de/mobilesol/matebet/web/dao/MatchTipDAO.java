package de.mobilesol.matebet.web.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dao.entity.MatchTipEntity;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;

public class MatchTipDAO {
	private static final Logger log = Logger
			.getLogger(MatchTipDAO.class.getName());
	private static final String TABLE = "matchtip_t";

	public void addMatchTips(int betId, int groupId, int userId,
			TipsByMatchMap m) {
		log.info("addMatchTip=" + m);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE,
				betId + "/" + groupId + "/" + userId);
		Entity e = toEntity(new Entity(key), betId, groupId, userId, m);
		e.setProperty("created", new Date());

		datastore.put(e);
	}

	public MatchTipDTO getMatchTips(int betId, int groupId, int userId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		List<Filter> fa = new ArrayList<Filter>();
		fa.add(new FilterPredicate("betId", FilterOperator.EQUAL, betId));
		if (groupId >= 0) {
			fa.add(new FilterPredicate("groupId", FilterOperator.EQUAL,
					groupId));
		}
		if (userId >= 0) {
			fa.add(new FilterPredicate("userId", FilterOperator.EQUAL, userId));
		}
		if (fa.size() > 1) {
			q.setFilter(new CompositeFilter(CompositeFilterOperator.AND, fa));
		} else {
			q.setFilter(fa.get(0));
		}

		PreparedQuery pq = datastore.prepare(q);

		MatchTipDTO ret = new MatchTipDTO();

		ret.betId = betId;
		ret.groupsByUser = new HashMap<Integer, TipsByMatchByGroupMap>();

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			MatchTipEntity a = toObject(it.next());

			TipsByMatchByGroupMap tm = ret.groupsByUser.get(a.userId);
			if (tm == null) {
				tm = new TipsByMatchByGroupMap();
				tm.groups = new HashMap<Integer, MatchTipDTO.TipsByMatchMap>();
				ret.groupsByUser.put(a.userId, tm);
			}
			tm.groups.putAll(a.groups);
		}

		return ret;
	}

	private Entity toEntity(Entity e, int betId, int groupId, int userId,
			TipsByMatchMap m) {
		e.setProperty("betId", betId);
		e.setProperty("groupId", groupId);
		e.setProperty("userId", userId);

		Gson gson = new Gson();
		e.setProperty("tips", gson.toJson(m));

		return e;
	}

	private MatchTipEntity toObject(Entity e) {
		MatchTipEntity a = new MatchTipEntity();
		a.betId = ((Number) e.getProperty("betId")).intValue();
		int groupId = ((Number) e.getProperty("groupId")).intValue();
		a.userId = ((Number) e.getProperty("userId")).intValue();

		a.groups = new HashMap<Integer, TipsByMatchMap>();

		String txt = (String) e.getProperty("tips");
		Gson gson = new Gson();
		TipsByMatchMap g = gson.fromJson(txt, TipsByMatchMap.class);

		a.groups.put(groupId, g);

		return a;
	}
}
