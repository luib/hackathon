package de.mobilesol.matebet.web.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.service.external.SyncOpenLigaDBService;

public class ImportHandler extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(ImportHandler.class.getName());

	private SyncOpenLigaDBService syncService = new SyncOpenLigaDBService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		try {
			String uri = req.getRequestURI();

			String league = uri.substring(uri.lastIndexOf("/") + 1);

			syncService.importLeague(Integer.parseInt(league));
		} catch (Exception e) {
			log.error("failed", e);
		}
	}
}
