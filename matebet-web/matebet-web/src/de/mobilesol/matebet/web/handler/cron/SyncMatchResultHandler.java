package de.mobilesol.matebet.web.handler.cron;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.service.external.SyncOpenLigaDBService;

public class SyncMatchResultHandler extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(SyncMatchResultHandler.class.getName());

	private BetFinishService betFinishService = new BetFinishService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

	}
}
