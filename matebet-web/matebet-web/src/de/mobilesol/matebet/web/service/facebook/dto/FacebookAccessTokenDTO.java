package de.mobilesol.matebet.web.service.facebook.dto;

public class FacebookAccessTokenDTO {

	public String access_token;
	public long expires_in;

	@Override
	public String toString() {
		return "[access_token=" + access_token + ", expires_in=" + expires_in
				+ "]";
	}
}
