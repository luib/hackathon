package de.mobilesol.matebet.web.service;

import java.util.List;

import javax.cache.Cache;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dao.BetClosedDAO;
import de.mobilesol.matebet.web.dao.StatisticDAO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.util.Util;

public class ClosedBetService {

	private static final Logger log = Logger
			.getLogger(ClosedBetService.class.getName());

	private BetClosedDAO closedDao = new BetClosedDAO();

	@SuppressWarnings("unchecked")
	public List<BetDTO> getClosedBets(int userId) {
		log.info("getClosedBets(" + userId + ")");

		final String key = "mb:closed:" + userId;
		Cache c = null;
		List<BetDTO> l;
		try {
			c = Util.getCache();
			l = (List<BetDTO>) c.get(key);
			if (l == null) {
				log.info("reading from db");
				l = closedDao.getClosedBets(userId);
				c.put(key, l);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			l = closedDao.getClosedBets(userId);
		}

		return l;
	}

	@SuppressWarnings("unchecked")
	public BetDTO getClosedBet(int betId, int userId) {
		log.info("getClosedBet(" + betId + "," + userId + ")");

		final String key = "mb:closed:" + betId + ":" + userId;
		Cache c = null;
		BetDTO bet;
		try {
			c = Util.getCache();
			bet = (BetDTO) c.get(key);
			if (bet == null) {
				log.info("reading from db");
				bet = closedDao.getBet(betId, userId);
				// TODO: check
				if (bet == null) {
					return null;
				}
				Util.addTeamUrls(bet);
				c.put(key, bet);
			} else {
				log.info("reading from cache");
			}
		} catch (Exception e) {
			log.error("failed", e);
			bet = closedDao.getBet(betId, userId);
			Util.addTeamUrls(bet);
		}

		return bet;
	}

	public void setArchived(int betId, int userId) {
		log.info("setArchived(" + betId + ", " + userId + ")");

		final String key1 = "mb:closed:" + userId;
		final String key2 = "mb:closed:" + betId + ":" + userId;

		Cache c = null;
		try {
			c = Util.getCache();
			c.remove(key1);
			c.remove(key2);
		} catch (Exception e) {
			log.error("removing cache failed", e);
		}

		closedDao.setArchived(betId, userId);
	}

	public void delete(int betId, int userId) {
		log.info("delete(" + betId + ", " + userId + ")");

		final String key1 = "mb:closed:" + userId;
		final String key2 = "mb:closed:" + betId + ":" + userId;

		Cache c = null;
		try {
			c = Util.getCache();
			c.remove(key1);
			c.remove(key2);
		} catch (Exception e) {
			log.error("removing cache failed", e);
		}

		BetDTO bet = getClosedBet(betId, userId);
		if (bet == null) {
			log.warn("bet has already been deleted");
			return;
		}

		log.info("bet status" + bet.status);
		if (bet.status == BetStatus.FINISHED) {
			updateStatistics(bet);
		}

		closedDao.delete(betId, userId);
	}

	private void updateStatistics(BetDTO bet) {
		log.info("update statistics for bet " + bet);
		new StatisticDAO().updateStaistic(bet);
	}
}
