package de.mobilesol.matebet.web.service.googleplus.dto;

import java.util.List;
import java.util.Map;

public class GoogleplusUserDTO {
	public String id;
	public String displayName;
	public Map<String, String> image;
	public List<Map<String, String>> emails;
	public AgeRangeFb ageRange;

	public static class AgeRangeFb {
		public Integer min, max;
	}

	@Override
	public String toString() {
		return "UserGp [displayName=" + displayName + ", image=" + image
				+ ", emails=" + emails + "]";
	}
}
