package de.mobilesol.matebet.web.service.twitter.dto;

public class TwitterAccessTokenDTO {
	public String oauth_token, oauth_token_secret, screen_name;
	public long user_id;
	public long x_auth_expires;

	@Override
	public String toString() {
		return "TwitterAccessTokenDTO [oauth_token=" + oauth_token
				+ ", oauth_token_secret=" + oauth_token_secret
				+ ", screen_name=" + screen_name + ", user_id=" + user_id
				+ ", x_auth_expires=" + x_auth_expires + "]";
	}
}
