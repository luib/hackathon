package de.mobilesol.matebet.web.service.twitter.dto;

import java.util.List;

public class TwitterFriendDTO {
	public long previous_cursor;
	public String previous_cursor_str;
	public long next_cursor;

	// public List<TwDTO> users;
	public List<Long> ids;

	@Override
	public String toString() {
		return "TwitterFriendDTO [previous_cursor=" + previous_cursor
				+ ", previous_cursor_str=" + previous_cursor_str
				+ ", next_cursor=" + next_cursor + ", ids=" + ids + "]";
	}

	public static class TwDTO {
		public long id;
		public String name, screen_name, profile_image_url;

		@Override
		public String toString() {
			return "TwDTO [id=" + id + ", name=" + name + ", screen_name="
					+ screen_name + ", profile_image_url=" + profile_image_url
					+ "]";
		}
	}
}
