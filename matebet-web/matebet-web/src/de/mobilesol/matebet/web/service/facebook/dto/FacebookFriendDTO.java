package de.mobilesol.matebet.web.service.facebook.dto;

import java.util.List;

public class FacebookFriendDTO {
	public List<Friend> data;
	public Paging paging;
	public Summary summary;

	public static class Friend {
		public long id;
		public String name;
	}

	public static class Paging {
		public Cursor cursor;
		public String next, previous;
	}

	public static class Cursor {
		public String before, after;
	}

	public static class Summary {
		public long total_count;
	}
}
