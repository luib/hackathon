package de.mobilesol.matebet.web.util;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.util.CalculatePointsUtil;

public class CalculatePointsUtilTest {

	@Test(expected = IllegalStateException.class)
	public void calculatePoints() {
		TipsByMatchMap myTips = new TipsByMatchMap();
		myTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		TipDTO tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		myTips.tips.put(1, tip);

		TipsByMatchMap resultTips = new TipsByMatchMap();
		new CalculatePointsUtil().calculatePointsByMatch(1, myTips, resultTips);
	}

	@Test
	public void calculatePointsWon() {
		TipsByMatchMap myTips = new TipsByMatchMap();
		myTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		TipDTO tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		myTips.tips.put(1, tip);

		TipsByMatchMap resultTips = new TipsByMatchMap();
		resultTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		resultTips.tips.put(1, tip);

		int res = new CalculatePointsUtil().calculatePointsByMatch(1, myTips,
				resultTips);

		Assert.assertEquals(res, 3);
	}

	@Test
	public void calculatePointsWon2() {
		TipsByMatchMap myTips = new TipsByMatchMap();
		myTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		TipDTO tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		myTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 2;
		tip.tipTeam2 = 0;
		myTips.tips.put(2, tip);

		TipsByMatchMap resultTips = new TipsByMatchMap();
		resultTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();

		tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		resultTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 0;
		tip.tipTeam2 = 2;
		resultTips.tips.put(2, tip);

		int res = new CalculatePointsUtil().calculatePointsByMatch(1, myTips,
				resultTips);
		Assert.assertEquals(res, 3);

		res = new CalculatePointsUtil().calculatePointsByMatch(2, myTips,
				resultTips);
		Assert.assertEquals(res, 0);
	}

	@Test
	public void calculatePointsTendency() {
		TipsByMatchMap myTips = new TipsByMatchMap();
		myTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		TipDTO tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		myTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 3;
		tip.tipTeam2 = 3;
		myTips.tips.put(2, tip);

		TipsByMatchMap resultTips = new TipsByMatchMap();
		resultTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();

		tip = new TipDTO();
		tip.tipTeam1 = 2;
		tip.tipTeam2 = 2;
		resultTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 3;
		tip.tipTeam2 = 3;
		resultTips.tips.put(2, tip);

		int res = new CalculatePointsUtil().calculatePointsByMatch(1, myTips,
				resultTips);
		Assert.assertEquals(res, 2);

		res = new CalculatePointsUtil().calculatePointsByMatch(2, myTips,
				resultTips);
		Assert.assertEquals(res, 3);

	}

	@Test
	public void calculatePointsLost() {
		TipsByMatchMap myTips = new TipsByMatchMap();
		myTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
		TipDTO tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 1;
		myTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 3;
		tip.tipTeam2 = 1;
		myTips.tips.put(2, tip);

		TipsByMatchMap resultTips = new TipsByMatchMap();
		resultTips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();

		tip = new TipDTO();
		tip.tipTeam1 = 1;
		tip.tipTeam2 = 2;
		resultTips.tips.put(1, tip);

		tip = new TipDTO();
		tip.tipTeam1 = 2;
		tip.tipTeam2 = 3;
		resultTips.tips.put(2, tip);

		int res = new CalculatePointsUtil().calculatePointsByMatch(1, myTips,
				resultTips);
		Assert.assertEquals(res, 0);
		res = new CalculatePointsUtil().calculatePointsByMatch(2, myTips,
				resultTips);
		Assert.assertEquals(res, 0);
	}

	@Test
	public void calculatePoints2() {
		CalculatePointsUtil util = new CalculatePointsUtil();
		BetDTO b = new BetDTO();
		// util.calculatePoints(b, null, null);
	}
}
