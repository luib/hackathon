package de.mobilesol.matebet.web.service.external;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;

public class SyncOpenLigaDBServiceTest {

	private SyncOpenLigaDBService service = new SyncOpenLigaDBService();

	@Test
	public void equalsInt() {
		Assert.assertTrue(SyncOpenLigaDBService.equals(3, 3));
		Assert.assertFalse(SyncOpenLigaDBService.equals(1, 3));
	}

	@Test
	public void equalsString() {
		Assert.assertTrue(SyncOpenLigaDBService.equals("3", "3"));
		Assert.assertFalse(SyncOpenLigaDBService.equals("1", "3"));
	}

	@Test
	public void equalsMatch() {
		MatchDTO m1 = new MatchDTO();
		m1.groupId = 1;
		m1.season = "2016";
		m1.matchdate = new Date();
		m1.team1.teamId = 27;
		m1.team2.teamId = 28;

		MatchDTO m2 = new MatchDTO();
		m2.groupId = 2;
		m2.season = "2016";
		m2.matchdate = m1.matchdate;
		m2.team1.teamId = 27;
		m2.team2.teamId = 28;

		Assert.assertTrue(SyncOpenLigaDBService.equals(m1, m1));
		Assert.assertTrue(SyncOpenLigaDBService.equals(m1, m2));

		m2.season = "2017";

		Assert.assertFalse(SyncOpenLigaDBService.equals(m1, m2));

		m2.season = "2016";
		m2.matchdate = new Date(System.currentTimeMillis() + 1000);

		Assert.assertFalse(SyncOpenLigaDBService.equals(m1, m2));
	}

	@Test
	public void equalsGroup() {
		GroupDTO g1 = new GroupDTO();
		g1.name = "group1";
		g1.season = "2016";

		GroupDTO g2 = new GroupDTO();
		g2.name = "group1";
		g2.season = "2016";

		Assert.assertTrue(SyncOpenLigaDBService.equals(g1, g1));
		Assert.assertTrue(SyncOpenLigaDBService.equals(g1, g2));
	}

	@Test
	public void equalsGroup2() {
		GroupDTO g1 = new GroupDTO();
		g1.groupId = 17750;
		g1.leagueId = 848;
		g1.groupOrderId = 1;
		g1.name = "1. Spieltag";
		g1.season = "2015";
		g1.firstMatchdate = new Date();

		GroupDTO g2 = new GroupDTO();
		g2.groupId = 17750;
		g2.leagueId = 848;
		g2.groupOrderId = 1;
		g2.name = "1. Spieltag";
		g2.season = "2015";
		g2.firstMatchdate = g1.firstMatchdate;

		Assert.assertTrue(SyncOpenLigaDBService.equals(g1, g1));
		Assert.assertTrue(SyncOpenLigaDBService.equals(g1, g2));
	}

	@Test
	public void unequalsGroup() {
		GroupDTO g1 = new GroupDTO();
		g1.name = "group1";
		g1.season = "2016";

		GroupDTO g2 = new GroupDTO();
		g2.name = "group";
		g2.season = "2016";

		Assert.assertFalse(SyncOpenLigaDBService.equals(g1, g2));
		g2.name = "group1";
		Assert.assertTrue(SyncOpenLigaDBService.equals(g1, g2));
		g2.firstMatchdate = new Date();
		Assert.assertFalse(SyncOpenLigaDBService.equals(g1, g2));
	}

	@Test
	public void deltaGroupEmpty() {
		Assert.assertEquals(
				SyncOpenLigaDBService.delta(Collections.<GroupDTO> emptyList(),
						Collections.<GroupDTO> emptyList()).size(), 0);
	}

	@Test
	public void deltaGroup() {
		GroupDTO g1 = new GroupDTO();
		g1.name = "group1";
		g1.season = "2016";

		GroupDTO g2 = new GroupDTO();
		g2.name = "group2";
		g2.season = "2016";

		Assert.assertEquals(
				SyncOpenLigaDBService.delta(Collections.singletonList(g1),
						Collections.singletonList(g1)).size(), 0);

		List<GroupDTO[]> gs = SyncOpenLigaDBService.delta(
				Collections.singletonList(g1), Collections.singletonList(g2));
		Assert.assertEquals(gs.size(), 1);
		Assert.assertEquals(gs.get(0)[0], g1);
		Assert.assertEquals(gs.get(0)[0].name, g1.name);
		Assert.assertEquals(gs.get(0)[1], g2);
		Assert.assertEquals(gs.get(0)[1].name, g2.name);

		gs = SyncOpenLigaDBService.delta(Collections.<GroupDTO> emptyList(),
				Collections.singletonList(g2));
		Assert.assertEquals(gs.size(), 1);
		Assert.assertEquals(gs.get(0)[0], null);
		Assert.assertEquals(gs.get(0)[1], g2);
		Assert.assertEquals(gs.get(0)[1].name, g2.name);

		gs = SyncOpenLigaDBService.delta(Collections.singletonList(g1),
				Collections.<GroupDTO> emptyList());
		Assert.assertEquals(gs.size(), 1);
		Assert.assertEquals(gs.get(0)[0], g1);
		Assert.assertEquals(gs.get(0)[0].name, g1.name);
		Assert.assertEquals(gs.get(0)[1], null);
	}

	@Test
	public void deltaGroup2() {
		GroupDTO g1 = new GroupDTO();
		g1.groupId = 1;
		g1.name = "group1";
		g1.season = "2016";

		GroupDTO g2 = new GroupDTO();
		g2.groupId = 2;
		g2.name = "group2";
		g2.season = "2016";

		GroupDTO g3 = new GroupDTO();
		g3.groupId = 3;
		g3.name = "group3";
		g3.season = "2016";

		List<GroupDTO> l1 = new ArrayList<GroupDTO>();
		l1.add(g1);
		l1.add(g2);
		List<GroupDTO> l2 = new ArrayList<GroupDTO>();
		l2.add(g2);
		l2.add(g3);

		List<GroupDTO[]> gs = SyncOpenLigaDBService.delta(l1, l2);
		Assert.assertEquals(gs.size(), 2);

		Assert.assertEquals(gs.get(0)[0], g1);
		Assert.assertEquals(gs.get(0)[0].name, g1.name);
		Assert.assertEquals(gs.get(0)[1], null);

		Assert.assertEquals(gs.get(1)[0], null);
		Assert.assertEquals(gs.get(1)[1], g3);
		Assert.assertEquals(gs.get(1)[1].name, g3.name);
	}
}
